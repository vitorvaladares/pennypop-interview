﻿public enum AnimationState
{
    atk1 = -1,
    idle = 0,
    walk = 1,
    hit = 2,
    death = 3,
}
