﻿using UnityEngine;
using System.Collections;

public class Credits : MonoBehaviour
{
    //This script shows the credits for the game
    //I had to use some sort of "delay" since the game was opening this and closing it on the same frame, because I use the "any key down" exit

    public GameObject goCredits;
    private bool _firstFrame;
    private MenuController _mc;

    public void Open()
    {
        _firstFrame = true;
        this.enabled = true;
        goCredits.SetActive(true);
    }

    void Start()
    {
        _mc = GameController.GetInstance().GetMenuController();
    }

    void Update()
    {
        //Here is the "delay"
        //I doubt that the player will notice...
        if (_firstFrame)
        {
            if (Input.anyKeyDown)
                return;
            else
                _firstFrame = false;
        }

        if (Input.anyKeyDown)
        {
            goCredits.SetActive(false);
            this.enabled = false;
            _mc.enabled = true;
            _mc.goOptions.SetActive(true);
        }
        
    }
}
