﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using LitJson;

public class WeatherAPI : MonoBehaviour
{

    //Since the API ask for and ID, I thought that it would be better to split those strings
    public string ExternalCall = "http://api.openweathermap.org/data/2.5/weather?q=San%20Francisco,us&appid=";
    public string AppId = "bd82977b86bf27fb59a04b61b657fb6f";

    public Text Response;
    public GameObject Tab, TxtMsg; //The txtMsg refers to the "press any key to exit" object

    private bool _finish, _watingCommand;
    private string _data;

    private MenuController _mc;

    public void Awake()
    {
        Tab.SetActive(false); //In case I forgot to close it before starting the game...

        _mc = GameController.GetInstance().GetMenuController();      
    }

    public void Open()
    {
        StartCoroutine(RoutineCallAPI());
    }
    private IEnumerator RoutineCallAPI()
    {
        Tab.SetActive(true);

        TxtMsg.SetActive(false);

        Response.text = "Fecthing for weather...";

        WWW www = new WWW(ExternalCall + AppId); //I like this WWW class from Unity, It's so easy to use!

        yield return www;

        if (!string.IsNullOrEmpty(www.error)) //I check if the API return some error
        {
            Response.text = "WWW Error: " + www.error;
        }
        else if (www.bytesDownloaded == 0) //Or if I can't reach it
        {
            Response.text = "Download Error: The method returned nothing";
        }
        else //If all is good, then I store the result and parse it on the update
        {
            _data = www.text;

            _finish = true;
        }

        TxtMsg.SetActive(true); ///Then I inform tha player, that he/she can exit the menu
    }

    public void Close()
    {
        Tab.SetActive(false);
        _mc.goOptions.SetActive(true);
        _mc.enabled = true;
    }

    private void Update()
    {
        if (_finish)
        {
            //When the API it's done and return the result (correctly) then I use a Json Parser to read it.

            _finish = false;

            try
            {
                var json = JsonMapper.ToObject(_data); //It's really neat!

                Response.text = json["weather"][0]["description"].ToString();
            }
            catch (System.Exception ex)
            {
                Response.text = ex.Message;
            }

            _watingCommand = true;
        }

        if (_watingCommand)
        {
            if (Input.anyKeyDown) //Any key down will return to the main menu
            {
                Close();
            }
        }
    }
}
