﻿using UnityEngine;
using System.Collections;

public class Instruction : MonoBehaviour
{
    //This script is just for the instructions screen. Nothing fancy here...
    //Just a "Any key down" close window situation...

    void Update()
    {
        if (Input.anyKeyDown)
        {
            this.enabled = false;
            Application.LoadLevel("SCN_Game");
        }
    }
}
