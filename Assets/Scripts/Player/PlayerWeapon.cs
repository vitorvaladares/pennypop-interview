﻿using UnityEngine;
using System.Collections;

public class PlayerWeapon : MonoBehaviour
{
    //A simple script to inform the parent when the hit occurs
    public PlayerCombat parent;
    public Transform ReferencePoint;
    private void OnTriggerEnter(Collider col)
    {
        parent.ColisionEvent(col, ReferencePoint.position);
    }
}
