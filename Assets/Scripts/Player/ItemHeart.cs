﻿using UnityEngine;
using System.Collections;

public class ItemHeart : MonoBehaviour
{
    //This class holds the functionality of the Hearts (lifes) spawned

    public bool fullHeart = true;
    public float rotationSpeed;
    public ParticleSystem particle;

    IEnumerator Start()
    {
        //When the heart spawns, it those this scale lerp (so it does not look boring)
        var t = 0F;
        var tMax = 1F;
        var localScale = this.transform.localScale;
        while (t < tMax)
        {
            t += Time.deltaTime;
            this.transform.localScale = Vector3.Lerp(Vector3.zero, localScale, t / tMax);
            yield return null;
        }
    }
    void OnTriggerEnter(Collider col)
    {
        //When the player grab it, It throws a particle
        var p = col.GetComponent<PlayerCombat>();

        if (p != null)
        {
            p.GainLife(fullHeart ? 2 : 1);

            if (particle != null)
            {
                var go = (GameObject) Instantiate(particle.gameObject, col.transform.position, Quaternion.identity);
                go.transform.SetParent(col.transform);
                Destroy(go.gameObject, 3);
            }

            Destroy(this.gameObject);
        }
    }
    private void Update()
    {
        //While it's on the wild, the heart spins
        this.transform.Rotate(Vector3.up * rotationSpeed * Time.deltaTime);
    }
}
