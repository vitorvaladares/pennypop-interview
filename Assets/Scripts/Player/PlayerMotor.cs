﻿using UnityEngine;
using System.Collections;

public class PlayerMotor : MonoBehaviour
{
    //This is the script responsable for the moviment of the player

    private CharAnimation _animation;
    private CharacterController _motor;

    public float walkSpeed;

    private bool _isWalking;

    private Vector3 _moveDirection, _currentDirection;

    private void Awake()
    {
        _motor = this.GetComponent<CharacterController>();
        _animation = this.GetComponent<CharAnimation>();
    }

    private void FixedUpdate()
    {
        //I split the gravity from the move direction, that way a jump implementation could be done more easily
        //The direction is normalized so it doesn't go faster on diagonal directions
        this._motor.SimpleMove(Time.deltaTime * _moveDirection.normalized * walkSpeed);
        this._motor.SimpleMove(Time.deltaTime * Physics.gravity.y * Vector3.up);
     //When the player is moving, it rotates foward that direction
        if (_isWalking)
            this.transform.LookAt(_currentDirection + this.transform.position);
    }
    
    private void Update()
    {
        //If the player can't walk (let's say, because it take a hit), then we need to clear the move vectors
        if (!_animation.CanWalk)
        {
            _moveDirection.x = _moveDirection.z = 0;

            _isWalking = false;

            return;
        }
        //I tried to imitate the moviment from Bastion, since the camera was close to it
        //What it does is always rotate the player foward the direction it want to go
        _moveDirection.x = Input.GetAxisRaw("Horizontal");

        _moveDirection.z = Input.GetAxisRaw("Vertical");
        //I slerp the rotation, for a better appearance
        _currentDirection = Vector3.Slerp(_currentDirection, _moveDirection, Time.deltaTime * 6);

        if (Mathf.Abs(_moveDirection.x) > 0 || Mathf.Abs(_moveDirection.z) > 0)
        {
            _isWalking = true;
            _animation.SetState(AnimationState.walk);
        }
        else if (_isWalking)
        {
            _isWalking = false;
            _animation.SetState(AnimationState.idle);
        }
    }

}
