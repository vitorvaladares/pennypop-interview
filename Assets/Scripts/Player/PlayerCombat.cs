﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class PlayerCombat : MonoBehaviour
{
    public GameObject ParticleHit;
    public Renderer[] mainRenderes;

    public int Health = 6;
    public int FullHealth { get; private set; }

    private CharAnimation _charAnimation;
    private GameUIController _uiController;
    private CameraController _cameraController;
    private EnemiesController _enemiesController;

    private List<EnemyAttack> _enemiesHited;

    void Awake()
    {
        _enemiesHited = new List<EnemyAttack>();
        _charAnimation = this.GetComponent<CharAnimation>();
        _uiController = GameController.GetInstance().GetUIController();
        _cameraController = GameController.GetInstance().GetCameraController();
        _enemiesController = GameController.GetInstance().GetEnemiesController();
        FullHealth = Health;
    }
    public void ColisionEvent(Collider col, Vector3 point)
    {
        //This function its called by the weapon, when it hits the target
        var enemy = col.GetComponent<EnemyAttack>();
        //It checks if the current target was already hited by this attack AND if the player is in fact attacking
        if (!_enemiesHited.Contains(enemy) && _charAnimation.CurrentState == AnimationState.atk1)
        {
            _enemiesHited.Add(enemy);

            enemy.TakeHit(this.transform.position);
            //Is plays a particle corresponding to the attack
            var go = Instantiate(ParticleHit.gameObject, point, Quaternion.identity);
            Destroy(go, 1);
        }
    }
    public void GainLife(int qnt)
    {
        //This function its called when the player touchs a heart
        Health+= qnt;

        if (Health > FullHealth)
            Health = FullHealth;

        _uiController.UpdateLifes();
    }
    public void TakeHit(Vector3 origin)
    {
        //When the character take hit, it turn to the origin of the damage (to sync with the animation)
        origin.y = this.transform.position.y;
        this.transform.LookAt(origin);
        //Then it blinks red
        StartCoroutine(BlinkRed());
        //Lose health
        Health--;
        _uiController.UpdateLifes();
        //Shakes the camera
        _cameraController.Shake();
        //An plays the corresponding animation
        if (Health > 0)
        {
            _charAnimation.SetState(AnimationState.hit);
        }
        else
        {
            //If it dies, then it goes on the death routine
            _charAnimation.SetState(AnimationState.death);
            _cameraController.DeathState();
            GameController.GetInstance().GetEnemiesController().GameOver();
            GetComponent<CharacterController>().enabled = false;
            GetComponent<PlayerMotor>().enabled = false;
        }
    }
    IEnumerator BlinkRed()
    {
        //This routine makes the materials blink differnt during some time, for feedback reasons
        //Just a yield time

        var t = .25F;

        for (int i = 0; i < mainRenderes.Length; i++)
        {
            mainRenderes[i].material.color = Color.red;    
        }

        while (t > 0)
        {
            t -= Time.deltaTime;

            yield return null;
        }

        for (int i = 0; i < mainRenderes.Length; i++)
        {
            mainRenderes[i].material.color = Color.white;
        }
 
    }
    void Update()
    {
        if (Health > 0) //...To avoid bad situations
        {
            if (_charAnimation.CanAtk) //If the player can attack and it does, then the attack animation goes on
            {
                if (Input.GetButtonDown("Atk1"))
                {
                    if (_enemiesController.TargetEnemy)
                    {
                        var pointToLook = _enemiesController.TargetEnemy.position;
                        pointToLook.y = this.transform.position.y;
                        this.transform.LookAt(pointToLook);
                    }
                    _enemiesHited = new List<EnemyAttack>();
                    _charAnimation.SetState(AnimationState.atk1);
                }
            }
        }
    }
}
