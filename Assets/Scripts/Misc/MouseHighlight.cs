﻿using UnityEngine;
using System.Collections;

public class MouseHighlight : MonoBehaviour
{

    public Renderer rendererToHighlight;

    private void Awake()
    {
        rendererToHighlight.enabled = false;
    }
    private void OnMouseEnter()
    {
        rendererToHighlight.enabled = true;
    }
    private void OnMouseExit()
    {
        rendererToHighlight.enabled = false;
    }
}
