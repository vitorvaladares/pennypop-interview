﻿using UnityEngine;
using System.Collections;

public class PlayRandomClip : MonoBehaviour
{
    //This script is used by the characters to play their sounds
    //The call for the sound is given by animations events

    //I created this small class to hold audio sources and clips
    //This way, all sound could have their own settings
    [System.Serializable]
    public class RandomAudioGroup
    {
        public AudioSource source;

        public AudioClip[] clips;
    }

    //I have this special condition to, so I can reuse the code for special ocasions
    //O those situatios, I randomize a clip and play it when the object is created
    public bool playFromAwake;
    public int awakeGroup;

    public RandomAudioGroup[] audioGroups;
    //For Reference Only
    /*
        step = 0,
        attack = 1,
        hit = 2,
        death = 3,
        idle = 4,
        fall = 5
    */
    void Awake()
    {
        if (playFromAwake)
        {
            PlayRandomFromGroup(awakeGroup);
        }
    }
    public void PlayRandomFromGroup(int group)
    {
        if (audioGroups.Length > group)
            audioGroups[group].source.PlayOneShot(audioGroups[group].clips[Random.Range(0, audioGroups[group].clips.Length)]);
    }
}
