﻿using UnityEngine;
using System.Collections;

public class CharAnimation : MonoBehaviour
{
    public Animator animator;

    //I use those clips, in order to compare and to know when they started and when they ended
    public AnimationClip clipAtk, clipHit, clipIdleIntro;

    ///If I had more states, I would probably use some sorte of flag, like the example below,
    ///but I thought it would not improve the code and would only make it complex
    ///Example:
    /*
     [Flags]
    public enum CharState
    {
	    none	= 0,
	    idle 	= 1 << 0, 
	    walk	= 1 << 1,
	    atk     = 1 << 2,
     }
    */

    //This will basically reflects the constrains drawn on the animator...
    //If the walk animation can be played, then the "canWalk" will probably be True
    public bool CanWalk { get; private set; }
    public bool CanAtk { get; private set; }
    public AnimationState CurrentState { get; private set; }
    public AnimationClip CurrentClip { get { return animator.GetCurrentAnimatorClipInfo(0)[0].clip; } }

    //This variables are used for me to return from temporary animations, like atk and hit
    private AnimationClip _newTempClip;
    private bool _initializingTempState, _onTempState;

    void Awake()
    {
        //The enemies must finish they intro before start walking
        CanWalk = clipIdleIntro == null;
        CanAtk = true;

        //Again, only for the enemies
        if (!CanWalk)
            StartCoroutine(Intro());
    }
    IEnumerator Intro()
    {
        //A simple yield routine to wait for the intro animation

        //I had to add this, because the animator wasn't starting from the begining...
        while (animator.GetCurrentAnimatorClipInfo(0).Length == 0)
            yield return null;

        while (CurrentClip == clipIdleIntro)
            yield return null;

        CanWalk = true;
    }

    //This is the routine where I set the animations
    public void SetState(AnimationState newState)
    {
        //To avoid unwanted loops, I did a simple check of the current state
        if (newState == CurrentState)
            return;

        CurrentState = newState;

        //I set the "State" parameter on the animator
        //This is the only place where I do this!
        animator.SetInteger("State", (int)newState);

        CanWalk = false;
        CanAtk = false;

        //Depending on the clip, I have to run a special sequence after playing.
        //This is to return to the idle state after playing temporary clips
        switch (newState)
        {
            case AnimationState.atk1:
                WaitForAnimation(clipAtk);
                break;
            case AnimationState.hit:
                WaitForAnimation(clipHit);
                break;
            case AnimationState.death:
                break;
            default:
                //By default, all other states can go to walk or to atk
                CanWalk = true;
                CanAtk = true;
                break;
        }
    }
    private void Update()
    {
        //The routine to wait for temp clips to end
        if (_onTempState)
        {
            //The first condition its for when the clip doesn't start instantaneously (because of exit time, and some other animator situations)
            if (_initializingTempState)
            {
                _initializingTempState = !(CurrentClip == _newTempClip);
            }
            else //Then it's just a matter of wait...
            {
                if (CurrentClip != _newTempClip)
                {
                    _onTempState = false;

                    if (animator.GetInteger("State") < 3)
                        SetState(AnimationState.idle);
                    else
                        SetState(AnimationState.death);
                }
            }
        }
    }
    private void WaitForAnimation(AnimationClip next)
    {
        _initializingTempState = true;
        _onTempState = true;
        _newTempClip = next;
    }

}
