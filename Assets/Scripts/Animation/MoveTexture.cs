﻿using UnityEngine;
using System.Collections;

public class MoveTexture : MonoBehaviour
{
    //This script it's use to move a texture
    //On this project, I used it on the red lines on the floor, that marks the Trigger for the enemies

    public Vector2 speed;

    private Material _mainMaterial;
    private Vector2 _offSet;
    private Vector2 _limit;

    void Start()
    {
        var renderer = this.GetComponent<Renderer>();

        _mainMaterial = Instantiate(renderer.material) as Material;

        renderer.material = _mainMaterial;
        //I need to calculate the limits, so I don't got until the infinite with these numbers...
        _limit = _mainMaterial.GetTextureScale("_MainTex");
        _limit = new Vector2(Mathf.Abs(_limit.x), Mathf.Abs(_limit.y));
    }

    void Update()
    {
        _offSet += speed * Time.deltaTime;

        if (_offSet.x > _limit.x)
            _offSet.x = 0;

        if (_offSet.y > _limit.y)
            _offSet.y = 0;

        _mainMaterial.SetTextureOffset("_MainTex", _offSet);
    }
}
