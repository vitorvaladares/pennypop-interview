﻿using UnityEngine;
using System.Collections;

public class EnemySounds : MonoBehaviour
{
    //A simple script to give a feedback when the enemy hits the floor
    public AudioSource source;
    public GameObject particle;

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.gameObject.CompareTag("Floor"))
        {
            GameController.GetInstance().GetCameraController().Shake(2);

            source.PlayOneShot(source.clip);
            var go = Instantiate(particle, this.transform.position - Vector3.up * .05F, Quaternion.Euler(90,0,0));
            Destroy(go, 3);
            Destroy(this);
        }
    }
}
