﻿using UnityEngine;
using System.Collections;

public class EnemyWeapon : MonoBehaviour
{
    //A simple script to inform the parent when the hit occurs
    public EnemyAttack parent;
    public Transform ReferencePoint;
    private void OnTriggerEnter(Collider col)
    {
        parent.ColisionEvent(col, ReferencePoint.position);
    }
}
