﻿using UnityEngine;
using System.Collections;

public class TriggerCollider : MonoBehaviour {

    //This is the script that informs the Enemies Controller when the player is inside the Trigger Zone
    private EnemiesController _ec;

    void Awake()
    {
        _ec = GameController.GetInstance().GetEnemiesController();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _ec.Trigger(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _ec.Trigger(false);
        }
    }
}
