﻿using UnityEngine;
using System.Collections;

public class EnemyFollow : MonoBehaviour
{
    //This script is responsable for moving the enemy around the stage
    //It chase down the Target until it is close enough to attack

    private Transform _target; // The player
    private CharAnimation _animation;
    private CharacterController _motor;
    private EnemiesController _ec;
    public bool CloseEnough { get; private set; }

    public float walkSpeed, minDistance, repathDelay;

    private float _currentDelay;

    private bool _chase;

    private Vector3 _moveDirection, _currentDirection;

    private void Awake()
    {
        _motor = this.GetComponent<CharacterController>();
        _animation = this.GetComponent<CharAnimation>();

        _target = GameController.GetInstance().GetPlayerController().Player.transform;

        _ec = GameController.GetInstance().GetEnemiesController();
        _ec.Add(this);
    }
    private void OnMouseEnter()
    {
        _ec.SetTarget(this.transform);
    }
    private void OnMouseExit()
    {
        _ec.SetTarget(null);
    }
    private void FixedUpdate()
    {
        if (_chase)
        {
            //During the chase, the enemy is always looking at the player
            this._motor.SimpleMove(Time.deltaTime * _moveDirection.normalized * walkSpeed);

            this.transform.LookAt(_currentDirection + this.transform.position);
        }
        //Even if the enemy is not following, the gravity still happens
        this._motor.SimpleMove(Time.deltaTime * Physics.gravity.y * Vector3.up);
    }
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        //When it collides with the player, it stops chasing, to save memory from the distance calculations
        if (hit.gameObject.CompareTag("Player"))
        {
            if (_chase)
            {
                _chase = false;
                _animation.SetState(AnimationState.idle);

                CloseEnough = false;
            }
        }
    }
    private void Update()
    {
        //During the update, if the enemy can walk, it calculates (from time to time) the distance it is from the player
        //If the distance is bigger than the minimun, then it goes chase the player
        //The delay during the distance calcs its to save memory
        if (_animation.CanWalk)
        {
            if (_chase)
            {
                _moveDirection = _target.position - this.transform.position;

                _currentDirection = Vector3.Slerp(_currentDirection, _moveDirection, Time.deltaTime * 6);
            }

            if (_currentDelay > 0)
            {
                _currentDelay -= Time.deltaTime;
            }
            else
            {
                _currentDelay = repathDelay; // this is the delay

                var distance = Vector3.Distance(_target.position, this.transform.position) ;

                CloseEnough = distance < minDistance;

                if (CloseEnough)
                {
                    if (_chase)
                    {
                        _chase = false;
                        _animation.SetState(AnimationState.idle);
                    }
                }
                else
                {
                    if (!_chase)
                    {
                        _chase = true;
                        _animation.SetState(AnimationState.walk);
                    }
                }
            }
        }
        else //When it can't move, we need to clear the move vectors
        {
            _moveDirection.x = _moveDirection.z = 0;

            _chase = false;

            _currentDelay = 0;
        }
    }
}
