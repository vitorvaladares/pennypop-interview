﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour
{
    private Transform _target; //The player
    private EnemyFollow _follow;
    private CharAnimation _charAnimation;
    private CameraController _cameraController;

    public int Health = 3;

    public float DelayBetweenAtks;
    public Renderer[] mainRenderes;
    public Material[] deathMaterials;
    public GameObject ParticleHit;

    private float _waitingToAttack;

    private bool _atingiu;

    private void Awake()
    {
        _follow = this.GetComponent<EnemyFollow>();
        _charAnimation = this.GetComponent<CharAnimation>();
        _cameraController = GameController.GetInstance().GetCameraController();
        _target = GameController.GetInstance().GetPlayerController().Player.transform;
    }
    public void ColisionEvent(Collider col, Vector3 point)
    {
        //This function its called by the weapon, when it hits the target
        //It checks if the current attack has already hited AND if this enemy is in fact attacking
        if (!_atingiu && _charAnimation.CurrentState == AnimationState.atk1)
        {
            _atingiu = true;

            col.GetComponent<PlayerCombat>().TakeHit(this.transform.position);
            //Is plays a particle corresponding to the attack
            var go = Instantiate(ParticleHit.gameObject, point, Quaternion.identity);
            Destroy(go, 1);
        }
    }
    public void TakeHit(Vector3 origin)
    {
        //When the character take hit, it turn to the origin of the damage (to sync with the animation)
        origin.y = this.transform.position.y;
        this.transform.LookAt(origin);
        //Then it blinks red
        StartCoroutine(BlinkYellow());
        //Lose health
        Health--;
        //Shakes the camera
        _cameraController.Shake();
        //An plays the corresponding animation
        if (Health > 0)
        {
            _charAnimation.SetState(AnimationState.hit);
        }
        else
        {
            //If it dies, then it goes on the death routine
            StartCoroutine(DeathRoutine());
            _charAnimation.SetState(AnimationState.death);
            _follow.enabled = false;

            GetComponent<CharacterController>().enabled = false;

            GameController.GetInstance().GetEnemiesController().Remove(_follow);
        }
    }
    IEnumerator DeathRoutine()
    {
        var t = 0F;
        var tMax = 3F;

        var newColor = Color.white;
        newColor.a = 0;

        for (int i = 0; i < mainRenderes.Length; i++)
        {
            mainRenderes[i].sharedMaterial = deathMaterials[i]; // It's risky, but Unity has a bug that force me to do this way...
        }

        while (t < tMax)
        {
            t += Time.deltaTime;
            var color = Color.Lerp(Color.white, newColor, t / tMax);

            for (int i = 0; i < mainRenderes.Length; i++)
            {
                mainRenderes[i].material.color = color;
            }

            yield return null;
        }

        Destroy(this.gameObject);
    }
    IEnumerator BlinkYellow()
    {
        //This routine makes the materials blink differnt during some time, for feedback reasons
        //Just a yield time

        var t = .25F;

        for (int i = 0; i < mainRenderes.Length; i++)
        {
            mainRenderes[i].material.color = Color.yellow;
        }
        

        while (t > 0)
        {
            t -= Time.deltaTime;

            yield return null;
        }

        for (int i = 0; i < mainRenderes.Length; i++)
        {
            mainRenderes[i].material.color = Color.white;
        }
    }
    void Update()
    {
        //If the enemy can attack, if it is close enough from the player and if it does not attacked recently, then the attack animation goes on
        if (_charAnimation.CanAtk && _follow.CloseEnough)
        {
            if (_waitingToAttack > 0)
            {
                _waitingToAttack -= Time.deltaTime;
                return;
            }
            else
            {
                this.transform.LookAt(_target.position);

                _atingiu = false;

                _charAnimation.SetState(AnimationState.atk1);

                _waitingToAttack = DelayBetweenAtks;
            }
        }
    }
}
