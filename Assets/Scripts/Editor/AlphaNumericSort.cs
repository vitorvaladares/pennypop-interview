﻿using UnityEngine;
using UnityEditor;
using System.Collections;

//I had this class before starting the project, but it's preaty basic
//It allows me to easily change the sort method used by unity on the editor...
public class AlphaNumericSort : BaseHierarchySort
{
    public override int Compare(GameObject lhs, GameObject rhs)
    {
        if (lhs == rhs) return 0;
        if (lhs == null) return -1;
        if (rhs == null) return 1;
        return EditorUtility.NaturalCompare(lhs.name, rhs.name);
    }
}