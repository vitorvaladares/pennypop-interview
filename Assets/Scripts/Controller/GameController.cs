﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class GameController : MonoBehaviour
{

    //I always like to use singleton on my games. It reduces the drag and drop on inspector and reduces the chances of erros (not to mention the memory) by using find....
    private static GameController _instance;

    private PlayerController _playerController;
    private CameraController _cameraController;
    private MenuController _menuController;
    private EnemiesController _enemiesController;
    private GameUIController _uiController;


    public EventSystem eventSystem { get; private set; }

    private void Awake()
    {
        eventSystem = EventSystem.current;
    }
    //The scripts bellow are a just part of the singleton routine.
    public GameUIController GetUIController()
    {
        if (_uiController == null)
        {
            _uiController = GetInstance().GetComponentInChildren<GameUIController>();
        }
        return _uiController;
    }
    public EnemiesController GetEnemiesController()
    {
        if (_enemiesController == null)
        {
            _enemiesController = GetInstance().GetComponentInChildren<EnemiesController>();
        }
        return _enemiesController;
    }
    public PlayerController GetPlayerController()
    {
        if (_playerController == null)
        {
            _playerController = GetInstance().GetComponentInChildren<PlayerController>();
        }
        return _playerController;
    }
    public CameraController GetCameraController()
    {
        if (_cameraController == null)
        {
            _cameraController = GetInstance().GetComponentInChildren<CameraController>();
        }
        return _cameraController;
    }
    public MenuController GetMenuController()
    {
        if (_menuController == null)
        {
            _menuController = GetInstance().GetComponentInChildren<MenuController>();
        }

        return _menuController;
    }
    //This one have to be static (and use Find)
    public static GameController GetInstance()
    {
        if (_instance == null)
        {
            _instance = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>(); //I have to use on find though...
        }

        return _instance;
    }
}
