﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class EnemiesController : MonoBehaviour
{
    //This script holds the enemies and count them for statistics


    //This list holds the enemies used during the game.
    //I usualy preffer to hold a class that represent a single enemy, and not just a part of it (e.g.: the script to move it)
    //But for this application, I believe that it is not that bad
    private List<EnemyFollow> enemiesFollowing = new List<EnemyFollow>();

    //The prefab of the enemies
    public GameObject EnemyPrefab;
    //The Heart Prefab
    public GameObject HeartPrefab;
    //The area where I can instanciate enemies
    public Transform RegionToInstanciate;

    //The delay for new enemies to be instantiated
    public float DelayInstanciate;

    //The number of enemies to be defeated until a heart appears
    public int EnemiesPerHeart;

    private bool _canInstanciate; //This tells me if the player is in the trigger area
    private float _delay; //The runtime instantiate delay
    private int _enemyCountToHeart; //The number of enemies defeated since the last heat was given

    //The number of enemies defeated
    public int EnemiesDefeated { get; private set; }
    public int EnemiesCreated { get; private set; }
    public Transform TargetEnemy { get; private set; }

    private Transform _enemiesHolder;
    public void SetTarget(Transform target)
    {
        TargetEnemy = target;
    }
    private void Awake()
    {
        _enemiesHolder = new GameObject("Enemies Holder").GetComponent<Transform>();
        _enemiesHolder.SetParent(this.transform);

        _enemiesHolder.localPosition = Vector3.zero;
    }
    //The enemy informs the controller about its self
    public void Add(EnemyFollow following)
    {
        enemiesFollowing.Add(following);
    }
    public void Remove(EnemyFollow following)
    {
        EnemiesDefeated++;

        //If the player defeted enough enemies, then he receives a heart
        _enemyCountToHeart++;

        if (_enemyCountToHeart >= EnemiesPerHeart)
        {
            _enemyCountToHeart = 0;

            Instantiate(HeartPrefab, following.transform.position, Quaternion.identity);
        }

        enemiesFollowing.Remove(following);
    }

    public void GameOver()
    {
        //When the game is over, I disabled the enemies
        for (int i = 0; i < enemiesFollowing.Count; i++)
        {
            enemiesFollowing[i].GetComponent<CharAnimation>().SetState(AnimationState.idle);
            enemiesFollowing[i].GetComponent<EnemyAttack>().enabled = false;

            enemiesFollowing[i].enabled = false;
        }

        this.enabled = false;
    }
    //A collider informs me, when the player is on the trigger area
    public void Trigger(bool on)
    {
        _canInstanciate = on;
    }
    private void Update()
    {
        //The Update is used to instantiate new enemies (in a given area)
        if (_canInstanciate)
        {
            _delay -= Time.deltaTime;

            if (_delay < 0)
            {
                _delay = DelayInstanciate;

                var go = Instantiate(EnemyPrefab);

                var x = Mathf.Lerp(RegionToInstanciate.position.x - RegionToInstanciate.localScale.x / 2,
                    RegionToInstanciate.position.x + RegionToInstanciate.localScale.x / 2, Random.value);
                var z = Mathf.Lerp(RegionToInstanciate.position.z - RegionToInstanciate.localScale.z / 2,
                    RegionToInstanciate.position.z + RegionToInstanciate.localScale.z / 2, Random.value);
                var y = RegionToInstanciate.position.y;

                go.transform.position = new Vector3(x, y, z);

                go.name = EnemyPrefab.name + "_" + EnemiesCreated;
                go.transform.SetParent(_enemiesHolder);

                EnemiesCreated++;
            }
        }
    }
}
