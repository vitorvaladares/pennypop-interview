﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuController : MonoBehaviour
{
    //This is the controller used on the Main Menu Scene
    //There is not much going around here besides an Special Intro and Exit

    public enum MenuOption
    {
        none = -1,
        startGame,
        fecthAPI,
        credits,
        last
    }

    public WeatherAPI weatherAPI;
    public Credits credits;

    public Image imgEnding;

    public RectTransform rtTitle, rtAuthor, rtSelectArrow;

    public RectTransform[] rtOptions, rtOptionsFake;

    public GameObject goOptions;

    public AudioSource source, bgSound;

    public AudioClip acChangeSelection, acSelect;

    private bool endingLevel, skipIntro;

    private MenuOption _option = MenuOption.none;

    private IEnumerator Start()
    {
        //The intro shows the title of the project, and then goes activating the other parts
        //Tha player can skip it if he wants to
        rtAuthor.gameObject.SetActive(false);
        goOptions.SetActive(false);

        var t = 0F;
        var tMax = 5F;
        var initialPos = Vector2.up * -1080;
        var finalPos = Vector2.up * -rtTitle.sizeDelta.y;

        while (t < tMax && !skipIntro)
        {
            t += Time.deltaTime;

            rtTitle.anchoredPosition = Vector2.Lerp(initialPos, finalPos, t / tMax);

            yield return null;
        }

        rtTitle.anchoredPosition = finalPos;

        t = 0;
        tMax = 1F;

        while (t < tMax && !skipIntro)
        {
            t += Time.deltaTime;
            yield return null;
        }

        rtAuthor.gameObject.SetActive(true);

        t = 0;
        tMax = 1F;

        while (t < tMax && !skipIntro)
        {
            t += Time.deltaTime;
            yield return null;
        }

        goOptions.SetActive(true);

        _option = 0;

        SetSelection();
    }

    private void SetSelection()
    {
        source.PlayOneShot(acChangeSelection);

        rtSelectArrow.anchoredPosition = rtOptionsFake[(int)_option].anchoredPosition - Vector2.right * rtOptionsFake[(int)_option].sizeDelta.x / 2;
    }

    // This method is used by the buttons on the game canvas
    // The method simple convert the received int into a corresponding enum value and call for the other method that use this kind of variable
    // I prefer this aproach, because it's easier to read
    public void BtMenu(int intOption) 
    {
        if (!goOptions.activeSelf)
            return;

        _option = (MenuOption)intOption;

        SetSelection();

        ConfirmSelection();
    }
    private IEnumerator EndLevel()
    {
        //The exit to the game involves a fade and a blinking on the current option (the same way as the game over menu does)
        endingLevel = true;

        imgEnding.gameObject.SetActive(true);

        var blink = true;
        var imgOption = rtOptions[(int)_option].GetComponent<Text>();

        imgOption.color = Color.clear;

        var t = 0F;
        var tMax = 3F;
        var mt = 0F;
        var mtMax = .25F;
        var vol = bgSound.volume;
        while (t < tMax)
        {
            t += Time.deltaTime;
            mt += Time.deltaTime;

            imgEnding.color = Color.Lerp(Color.clear, Color.black, t / tMax);
            bgSound.volume = Mathf.Lerp(vol, 0, t / tMax);

            if (mt > mtMax)
            {
                mt = 0F;
                blink = !blink;
                imgOption.color = blink ? Color.clear : Color.red;
            }

            yield return null;
        }

        Application.LoadLevel("SCN_Instructions"); //Although  it may lead to error caused by bad writting, I like to use the name of the scene because it ended up being easier to read
    }
    private void ConfirmSelection()
    {
        source.PlayOneShot(acSelect);
        // The Weather API and the Credits sub menu are hadled in other scripts
        switch (_option)
        {
            case MenuOption.none:
                break;
            case MenuOption.startGame:
                StartCoroutine(EndLevel());
                break;
            case MenuOption.fecthAPI:
                weatherAPI.Open(); 

                goOptions.SetActive(false);
                this.enabled = false;
                break;
            case MenuOption.credits:
                credits.Open();

                goOptions.SetActive(false);
                this.enabled = false;
                break;
            default:
                break;
        }
    }
    private void Update()
    {
        if (endingLevel)
            return;

        if (_option > MenuOption.none)
        {
            //This is an improved menu compared to the one used on the game over, since it can escalate easily

            if (Input.GetButtonDown("Vertical"))
            {
                var change = Input.GetAxisRaw("Vertical");

                if (change < 0)
                {
                    _option++;

                    if (_option < MenuOption.last)
                        SetSelection();
                    else
                        _option--;
                }
                else
                {
                    _option--;

                    if (_option > MenuOption.none)
                        SetSelection();
                    else
                        _option++;
                }
            }

            if (Input.GetButtonDown("Confirm"))
            {
                ConfirmSelection();
            }
        }
        else
        {
            if (Input.GetButtonDown("Confirm"))
            {
                skipIntro = true;
            }
        }
    }
}
