﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameUIController : MonoBehaviour
{
    //This script controlls the UI during tha game
    //Basically it creates and controlls the health display (the hearts) and the game over menu

    public Image imgHearthBorder, imgHeart, imgEnding;

    public GameObject goMenuGameOver;

    public RectTransform rtSelectArrow, rtGameOver;

    public RectTransform[] rtOptionsGameOver;

    public Text txtResults;

    public AudioSource source;

    public AudioClip acChangeSelection, acSelect;

    private GameOverOptions _option = GameOverOptions.none;

    //It holds a reference for the player, so it can create and display the health
    private PlayerCombat _player;

    private Image[] _imgHearts;

    //This boolean avoids double clicks after the player has chosen an option
    private bool _endingLevel;

    public enum GameOverOptions
    {
        none = -1,
        TryAgain,
        MainMenu
    }

    // Use this for initialization
    void Start()
    {
        _player = GameController.GetInstance().GetPlayerController().Player.GetComponent<PlayerCombat>();

        //The health display its composed by groups of heart borders and hearts (full filled or not)
        //Every two life points its one heart

        int noOfHearths = _player.FullHealth / 2;

        if (_player.FullHealth % 2F > 0)
            noOfHearths++;

        var size = imgHearthBorder.rectTransform.sizeDelta.x;
        var comp = 10;

        _imgHearts = new Image[noOfHearths];
        _imgHearts[0] = imgHeart;

        for (int i = 1; i < noOfHearths; i++)
        {
            var heartborder = Instantiate(imgHearthBorder);
            heartborder.transform.SetParent(imgHearthBorder.transform.parent);
            heartborder.transform.localScale = imgHearthBorder.transform.localScale;//Very important, if you forget about it, the scale go nuts
            heartborder.rectTransform.anchoredPosition = Vector2.right * (i * size + i * comp);
            heartborder.name = "imgHearthBorder_" + i;

            var heart = Instantiate(imgHeart);
            heart.transform.SetParent(imgHeart.transform.parent);
            heart.transform.localScale = imgHeart.transform.localScale;//Very important, if you forget about it, the scale go nuts
            heart.rectTransform.anchoredPosition = heartborder.rectTransform.anchoredPosition;
            heart.name = "imgHearth_" + i;
            _imgHearts[i] = heart;
        }

        UpdateLifes();
    }

    public void UpdateLifes()
    {
        //Every two life points its one heart
        //For this routine, i filled all of them and then go erasing from the back
        for (int i = 0; i < _imgHearts.Length; i++)
        {
            _imgHearts[i].fillAmount = 1;
        }

        for (int i = _player.Health; i < _player.FullHealth; i++)
        {
            _imgHearts[i / 2].fillAmount = 0;
        }

        if (_player.Health % 2F > 0)
            _imgHearts[_player.Health / 2].fillAmount = .5F;
    }
    //Called from the camera death routine
    public void ShowGameOverMenu()
    {
        goMenuGameOver.SetActive(true);

        SetSelection(0);
    }
    //This one is for when the player changes it's option
    private void SetSelection(int op)
    {
        _option = (GameOverOptions) op;

        source.PlayOneShot(acChangeSelection);

        rtSelectArrow.anchoredPosition = rtOptionsGameOver[op].anchoredPosition - Vector2.right * rtOptionsGameOver[op].sizeDelta.x / 2;
    }
    //And this one one it accepted the option
    private void ConfirmSelection()
    {
        source.PlayOneShot(acSelect);

        StartCoroutine(EndLevel());
    }
    private IEnumerator EndLevel()
    {
        //This coroutine is used after the player has chosen an option on the menu
        //It does a fade while the option blinks red
        _endingLevel = true;

        imgEnding.gameObject.SetActive(true);

        var blink = true;
        var imgOption = rtOptionsGameOver[(int)_option].GetComponent<Text>();

        imgOption.color = Color.clear;

        var t = 0F;
        var tMax = 3F;
        var mt = 0F;
        var mtMax = .25F;

        while (t < tMax)
        {
            t += Time.deltaTime;
            mt += Time.deltaTime;

            imgEnding.color = Color.Lerp(Color.clear, Color.black, t / tMax);

            if (mt > mtMax)
            {
                mt = 0F;
                blink = !blink;
                imgOption.color = blink ? Color.clear : Color.red;
            }

            yield return null;
        }
        //...Then it does what the player wanted
        switch (_option)
        {
            case GameOverOptions.none:
                break;
            case GameOverOptions.TryAgain:
                Application.LoadLevel("SCN_Game");
                break;
            case GameOverOptions.MainMenu:
                Application.LoadLevel("SCN_MainMenu");
                break;
            default:
                break;
        }
    }
    //Used by the player, if it opts to use the mouse
    public void BtSelection(int option)
    {
        SetSelection(option);
        ConfirmSelection();
    }
    private void Update()
    {
        if (_endingLevel)
            return;

        //A simple input command menu
        if (_option > GameOverOptions.none)
        {
            if (Input.GetButtonDown("Horizontal"))
            {
                var change = Input.GetAxisRaw("Horizontal");
                //Since I only have 2 options, I did it this way, but on the main menu I used a better aproach (wich can grow better)
                if (change > 0)
                {
                    SetSelection(1);
                }
                else
                {
                    SetSelection(0);
                }
            }

            if (Input.GetButtonDown("Confirm"))
            {
                ConfirmSelection();
            }
        }
    }
}
