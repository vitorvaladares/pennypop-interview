﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraController : MonoBehaviour
{
    //This script controlls everything related to the game camera

    //The distance from the player
    public Vector3 distanceDefault;
    //A reference for the main camera (which is the only one that do something...)
    //I could simples use the Camera.MainCamera, but this way I avoid getting the wrong one, for some reason
    public Camera mainCamera;

    //This gameobject is use to blink everytime the player hits the enemys or the other way around
    //It gives a nice sense of impact :)
    public GameObject whiteWall;
    //This image is used when the game starts, for fade reasons
    public Image imgOpening;

    //Those values determine how long and how much the camera will shake after a hit
    public float ShakeDuration;
    public float ShakeIntensity;
    private float _shake; //And this one it's for the realtime count

    //And these are used for the script to start or interrupt the Death Animation Sequence
    private bool _inDeathRoutine, _skipDeathAnimation;

    //The reference of the player, since the camera needs to follow it
    private Transform _player;

    //TODO: Mudar de lugar
    //The session clock
    //This is not the best place for it, I know, but since I use it here, it becomes more practicall
    public float SessionDuration { get; private set; }

    //The camera has a special intro, so I used the Start function to it
    IEnumerator Start()
    {
        _player = GameController.GetInstance().GetPlayerController().Player.transform;

        mainCamera.transform.position = _player.position + distanceDefault;

        SessionDuration = 0F;

        //The intro is just a fade combined with a zoom out (Lerping on the Camera orthographicSize)

        var t = 0F;
        var tMax = 3F;
        var os = mainCamera.orthographicSize;

        mainCamera.orthographicSize = 1;

        imgOpening.gameObject.SetActive(true);

        while (t < tMax)
        {
            t += Time.deltaTime;

            mainCamera.orthographicSize = Mathf.Lerp(1, os, t / tMax);
            imgOpening.color = Color.Lerp(Color.black, Color.clear, t / tMax);
            yield return null;
        }

        imgOpening.gameObject.SetActive(false);
    }
    private void Update()
    {
        //Since the Death Animation can be a little to long, I implement a skip
        if (_inDeathRoutine)
        {
            if (Input.GetButtonDown("Confirm"))
            {
                _skipDeathAnimation = true;
            }
        }
        else //Obviously, the death animation those not count on the duration of the session
        {
            SessionDuration += Time.deltaTime;
        }
    }
    private void FixedUpdate()
    {
        if (_inDeathRoutine)
            return;

        //I used a slerp, instead of just set the position, because it gives a better fell
        mainCamera.transform.position = Vector3.Slerp(mainCamera.transform.position, _player.position + distanceDefault, Time.deltaTime * 3);

        //Tha shake routine its just a randomize position during some time
        if (_shake > 0)
        {
            mainCamera.transform.position += UnityEngine.Random.insideUnitSphere * ShakeIntensity * Time.deltaTime;
            _shake -= Time.deltaTime;
        }
    }
    //The player script informs the camera controller when to start the death routine
    public void DeathState()
    {
        _inDeathRoutine = true;
        StartCoroutine(DeathRoutine());
    }
    IEnumerator DeathRoutine()
    {
        //The death animation it's a sequence of small events

        var t = 0F;
        var tMax = 5F;
        var initialSize = mainCamera.orthographicSize;
        var newPos = new Vector3(_player.position.x, _player.position.x + distanceDefault.y, _player.position.z - .5F);
        var light = _player.GetComponentInChildren<Light>();
        var lightInt = light.intensity;

        //Firts, the camera flies to the player and also zoom in
        while (t < tMax && !_skipDeathAnimation)
        {
            t += Time.deltaTime;
            light.intensity = Mathf.Lerp(lightInt, 0, t / tMax);
            mainCamera.orthographicSize = Mathf.Lerp(initialSize, 1.5F, t / tMax);
            mainCamera.transform.LookAt(_player);
            mainCamera.transform.position = Vector3.Lerp(distanceDefault, newPos, t / tMax);
            yield return null;
        }

        light.intensity = 0;
        mainCamera.orthographicSize = 1.5F;
        mainCamera.transform.position = newPos;
        mainCamera.transform.LookAt(_player);

        //Then I activate the Game Over Screen (wich comes with its own sound affect)
        var ui = GameController.GetInstance().GetUIController();

        ui.rtGameOver.gameObject.SetActive(true);

        t = 0F;
        tMax = 1F;
        //Then a delay...
        while (t < tMax && !_skipDeathAnimation)
        {
            t += Time.deltaTime;

            yield return null;
        }

        t = 0F;
        tMax = 2F;
        //And then I move the Game Over to the top of the screen
        while (t < tMax && !_skipDeathAnimation)
        {
            t += Time.deltaTime;

            ui.rtGameOver.anchoredPosition = Vector2.up * Mathf.Lerp(0, ui.rtGameOver.sizeDelta.y / 2, t / tMax);

            yield return null;
        }

        ui.rtGameOver.anchoredPosition = Vector2.up * ui.rtGameOver.sizeDelta.y / 2;

        //Show the results
        ui.txtResults.gameObject.SetActive(true);
        ui.txtResults.text = ui.txtResults.text.Replace("#t", ((int)SessionDuration).ToString());
        ui.txtResults.text = ui.txtResults.text.Replace("#e", GameController.GetInstance().GetEnemiesController().EnemiesDefeated.ToString());

        t = 0F;
        tMax = 2F;
        //Anothe delay...
        while (t < tMax && !_skipDeathAnimation)
        {
            t += Time.deltaTime;

            yield return null;
        }
        //And turn on the game over menu, wich is handled on the UI Controller
        ui.ShowGameOverMenu();
    }
    //I combined the shake with the blink, because of the impact feel
    public void Shake(int mulitplier = 1)
    {
        _shake = ShakeDuration * mulitplier;

        StartCoroutine(QuickBlink());
    }
    //Here is the blik used during the hits
    //Again, just a yield time
    IEnumerator QuickBlink()
    {
        var t = .05F;

        whiteWall.SetActive(true);

        while (t > 0)
        {
            t -= Time.deltaTime;
            yield return null;
        }

        whiteWall.SetActive(false);
    }
}
